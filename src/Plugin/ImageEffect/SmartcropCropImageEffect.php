<?php
/**
 * @file
 * Contains \Drupal\smartcrop\Plugin\ImageEffect\SmartcropCropImageEffect.
 */

namespace Drupal\smartcrop\Plugin\ImageEffect;

use Drupal\Core\Image\ImageInterface;
use Drupal\image\Plugin\ImageEffect\ResizeImageEffect;

/**
 * Crops an image resource.
 *
 * @ImageEffect(
 *   id = "smartcrop_image_crop",
 *   label = @Translation("Smart Crop"),
 *   description = @Translation("Similar to ""Crop"", but preserves the portion of the image with the most entropy.")
 * )
 */
class SmartcropCropImageEffect extends ResizeImageEffect {

  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image) {
    if (!$image->apply('smart_crop', array('width' => $this->configuration['width'], 'height' => $this->configuration['height']))) {
      $this->logger->error('Image crop failed using the %toolkit toolkit on %path (%mimetype, %dimensions)', array('%toolkit' => $image->getToolkitId(), '%path' => $image->getSource(), '%mimetype' => $image->getMimeType(), '%dimensions' => $image->getWidth() . 'x' . $image->getHeight()));
      return FALSE;
    }
    return TRUE;
  }

}
