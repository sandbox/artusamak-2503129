<?php
/**
 * @file
 * Contains \Drupal\smartcrop\Plugin\ImageEffect\SmartcropScaleAndCropImageEffect.
 */

namespace Drupal\smartcrop\Plugin\ImageEffect;

use Drupal\Core\Image\ImageInterface;
use Drupal\image\Plugin\ImageEffect\ResizeImageEffect;

/**
 * Scales and crops an image resource.
 *
 * @ImageEffect(
 *   id = "smartcrop_image_scale_and_crop",
 *   label = @Translation("Scale and Smart Crop"),
 *   description = @Translation("Similar to ""Scale And Crop"", but preserves the portion of the image with the most entropy.")
 * )
 */
class SmartcropScaleAndCropImageEffect extends ResizeImageEffect {

  /**
   * {@inheritdoc}
   */
  public function applyEffect(ImageInterface $image) {
    if (!$image->apply('smart_scale_and_crop', array('width' => $this->configuration['width'], 'height' => $this->configuration['height']))) {
      $this->logger->error('image', 'Image scale failed using the %toolkit toolkit on %path (%mimetype, %dimensions)', array('%toolkit' => $image->getToolkitId(), '%path' => $image->getSource(), '%mimetype' => $image->getMimeType(), '%dimensions' => $image->getWidth() . 'x' . $image->getHeight()));
      return FALSE;
    }
    return TRUE;
  }

}
