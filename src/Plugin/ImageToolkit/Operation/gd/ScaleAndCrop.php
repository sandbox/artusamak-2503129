<?php

/**
 * @file
 * Contains \Drupal\smartcrop\Plugin\ImageToolkit\Operation\gd\ScaleAndCrop.
 */

namespace Drupal\smartcrop\Plugin\ImageToolkit\Operation\gd;

use Drupal\Component\Utility\SafeMarkup;
use Drupal\system\Plugin\ImageToolkit\Operation\gd\Scale;

/**
 * Defines GD2 Scale and crop operation.
 *
 * @ImageToolkitOperation(
 *   id = "gd_scale_and_crop",
 *   toolkit = "gd",
 *   operation = "scale_and_crop",
 *   label = @Translation("Scale and crop"),
 *   description = @Translation("Scales an image to the exact width and height given. This plugin achieves the target aspect ratio by cropping the original image equally on both sides, or equally on the top and bottom. This function is useful to create uniform sized avatars from larger images.")
 * )
 */
class ScaleAndCrop extends Scale {

  /**
   * {@inheritdoc}
   */
  protected function execute(array $arguments = array()) {
    // @todo reproduce image_gd_smartcrop_scale() behaviour.
  }

}
