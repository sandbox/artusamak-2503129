<?php

/**
 * @file
 * Contains \Drupal\smartcrop\Plugin\ImageToolkit\Operation\gd\SmartCrop.
 */

namespace Drupal\smartcrop\Plugin\ImageToolkit\Operation\gd;

use Drupal\system\Plugin\ImageToolkit\Operation\gd\Resize;

/**
 * Defines GD2 Smart Crop operation.
 *
 * @ImageToolkitOperation(
 *   id = "gd_smart_crop",
 *   toolkit = "gd",
 *   operation = "smart_crop",
 *   label = @Translation("Smart Crop"),
 *   description = @Translation("Similar to ""Crop"", but preserves the portion of the image with the most entropy.")
 * )
 */
class Crop extends Resize {
  
  /**
   * {@inheritdoc}
   */
  protected function execute(array $arguments) {
    $image = $this->getToolkit()->getResource();
    $dx = imagesx($image) - min(imagesx($image), $arguments['width']);
    $dy = imagesy($image) - min(imagesy($image), $arguments['height']);
    $left = $top = 0;
    $left_entropy = $right_entropy = $top_entropy = $bottom_entropy = 0;
    $right = imagesx($image);
    $bottom = imagesy($image);

    // Slice from left and right edges until the correct width is reached.
    while ($dx) {
      $slice = min($dx, 10);

      // Calculate the entropy of the new slice.
      if (!$left_entropy) {
        $left_entropy = $this->entropySlice($left, $top, $slice, imagesy($image));
      }
      if (!$right_entropy) {
        $right_entropy = $this->entropySlice($right - $slice, $top, $slice, imagesy($image));
      }

      // Remove the lowest entropy slice.
      if ($left_entropy >= $right_entropy) {
        $right -= $slice;
        $right_entropy = 0;
      }
      else {
        $left += $slice;
        $left_entropy = 0;
      }
      $dx -= $slice;
    }

    // Slice from the top and bottom edges until the correct width is reached.
    while ($dy) {
      $slice = min($dy, 10);

      // Calculate the entropy of the new slice.
      if (!$top_entropy) {
        $top_entropy = $this->entropySlice($left, $top, $arguments['width'], $slice);
      }
      if (!$bottom_entropy) {
        $bottom_entropy = $this->entropySlice($left, $bottom - $slice, $arguments['width'], $slice);
      }

      // Remove the lowest entropy slice.
      if ($top_entropy >= $bottom_entropy) {
        $bottom -= $slice;
        $bottom_entropy = 0;
      }
      else {
        $top += $slice;
        $top_entropy = 0;
      }
      $dy -= $slice;
    }

    // Finally, crop the image using the coordinates found above.
    $data = array(
      'width' => $right - $left,
      'height' => $bottom - $top,
      'extension' => image_type_to_extension($this->getToolkit()->getType(), FALSE),
      'transparent_color' => $this->getToolkit()->getTransparentColor()
    );
    if ($this->getToolkit()->apply('create_new', $data)) {
      if (imagecopy($this->getToolkit()->getResource(), $image, 0, 0, $left, $top, $right - $left, $bottom - $top)) {
        imagedestroy($image);
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Compute the entropy of an image slice.
   *
   * @param $x Starting X coordinate.
   * @param $y Starting Y coordinate.
   * @param $width The width of the slice.
   * @param $height The height of the slice.
   * @return float
   */
  protected function entropySlice($x, $y, $width, $height) {
    $image = $this->getToolkit()->getResource();
    $data = array(
      'width' => $width,
      'height' => $height,
      'extension' => image_type_to_extension($this->getToolkit()->getType(), FALSE),
      'transparent_color' => $this->getToolkit()->getTransparentColor()
    );
    if ($this->getToolkit()->apply('create_new', $data)) {
      if (imagecopy($this->getToolkit()->getResource(), $image, 0, 0, $x, $y, $width, $height)) {
        $entropy = $this->entropy();
        // Reset the original resource.
        $this->getToolkit()->setResource($image);
        return $entropy;
      }
    }
  }

  /**
   * Compute the entropy of an image, defined as -sum(p.*log2(p)).
   *
   * @return float The entropy of the image.
   */
  protected function entropy() {
    $histogram = $this->histogram();
    $histogram_size = array_sum($histogram);
    $entropy = 0;
    foreach ($histogram as $p) {
      if ($p == 0) {
        continue;
      }
      $p = $p / $histogram_size;
      $entropy += $p * log($p, 2);
    }
    return $entropy * -1;
  }

  /**
   * Compute a histogram of an image.
   *
   * @return array histogram as an array.
   */
  protected function histogram() {
    $histogram = array_fill(0, 768, 0);
    for ($i = 0; $i < imagesx($this->getToolkit()->getResource()); $i++) {
      for ($j = 0; $j < imagesy($this->getToolkit()->getResource()); $j++) {
        $rgb = imagecolorat($this->getToolkit()->getResource(), $i, $j);
        $r = ($rgb >> 16) & 0xFF;
        $g = ($rgb >> 8) & 0xFF;
        $b = $rgb & 0xFF;
        $histogram[$r]++;
        $histogram[$g + 256]++;
        $histogram[$b + 512]++;
      }
    }
    return $histogram;
  }

}
